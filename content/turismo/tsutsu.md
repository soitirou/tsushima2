+++
title = 'Cabo de Tsutsu'
date = 2023-10-09T15:01:11-03:00
draft = false
+++

No extremo sul de Tsushima encontra-se o farol de luz de Tsutsu e a área circundante é um parque. O parque tem um caminho de passeio, uma área de descanso e um observatório de onde pode ser visto as falésias íngremes.

![Cabo de Tsutsu](/images/tsutsu.jpeg)
Cabo de Tsutsu

![Caminho de passeio](/images/tsutsu2.jpeg)
Caminho de passeio sobre o Cabo de Tsutsu

![Farol de luz de Tsutsu](/images/tsutsufarol.jpg)
Farol de luz de Tsutsu